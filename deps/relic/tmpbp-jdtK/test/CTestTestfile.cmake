# CMake generated Testfile for 
# Source directory: /home/nikita/openabe/deps/relic/relic-toolkit-0.4.1h/test
# Build directory: /home/nikita/openabe/deps/relic/tmpbp-jdtK/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_bn "/home/nikita/openabe/deps/relic/tmpbp-jdtK/bin/test_bn")
add_test(test_dv "/home/nikita/openabe/deps/relic/tmpbp-jdtK/bin/test_dv")
add_test(test_fp "/home/nikita/openabe/deps/relic/tmpbp-jdtK/bin/test_fp")
add_test(test_fpx "/home/nikita/openabe/deps/relic/tmpbp-jdtK/bin/test_fpx")
add_test(test_ep "/home/nikita/openabe/deps/relic/tmpbp-jdtK/bin/test_ep")
add_test(test_epx "/home/nikita/openabe/deps/relic/tmpbp-jdtK/bin/test_epx")
add_test(test_pc "/home/nikita/openabe/deps/relic/tmpbp-jdtK/bin/test_pc")
add_test(test_pp "/home/nikita/openabe/deps/relic/tmpbp-jdtK/bin/test_pp")
add_test(test_md "/home/nikita/openabe/deps/relic/tmpbp-jdtK/bin/test_md")
add_test(test_rand "/home/nikita/openabe/deps/relic/tmpbp-jdtK/bin/test_rand")
add_test(test_core "/home/nikita/openabe/deps/relic/tmpbp-jdtK/bin/test_core")
